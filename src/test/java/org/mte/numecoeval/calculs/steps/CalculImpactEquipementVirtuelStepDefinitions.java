package org.mte.numecoeval.calculs.steps;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.fr.Alors;
import io.cucumber.java.fr.Soit;
import org.mte.numecoeval.calculs.domain.data.demande.DemandeCalculImpactEquipementVirtuel;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactEquipementVirtuelService;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactEquipementVirtuelServiceImpl;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class CalculImpactEquipementVirtuelStepDefinitions {

    DemandeCalculImpactEquipementVirtuel demandeCalculCourante;
    
    @Soit("Une demande de calcul pour un équipement virtuel tel que")
    public void initDemandeCalculEquipementVirtuel(DemandeCalculImpactEquipementVirtuel demande) {
        demandeCalculCourante = demande;
    }

    @Alors("l'impact d'équipement virtuel résultant est tel que")
    public void checkImpactEquipementVirtuel(DataTable dataTable) {
        CalculImpactEquipementVirtuelService service = new CalculImpactEquipementVirtuelServiceImpl(new ObjectMapper());
        var result = assertDoesNotThrow(() -> service.calculerImpactEquipementVirtuel(demandeCalculCourante));

        assertNotNull(result);
        Map<String, String> entry = dataTable.entries().get(0);
        assertNotNull(entry);

        DataTableTypeDefinitions.assertStringIfAvailable(result.getVersionCalcul(), entry, "versionCalcul");
        DataTableTypeDefinitions.assertStringIfAvailable(result.getNomLot(), entry, "nomLot");
        DataTableTypeDefinitions.assertDateIfAvailable(result.getDateLot(), entry, "dateLot");
        DataTableTypeDefinitions.assertStringIfAvailable(result.getNomOrganisation(), entry, "nomOrganisation");
        DataTableTypeDefinitions.assertStringIfAvailable(result.getNomEntite(), entry, "nomEntite");
        DataTableTypeDefinitions.assertStringIfAvailable(result.getCritere(), entry, "critere");
        DataTableTypeDefinitions.assertStringIfAvailable(result.getEtapeACV(), entry, "etapeACV");
        DataTableTypeDefinitions.assertStringIfAvailable(result.getStatutIndicateur(), entry, "statutIndicateur");
        DataTableTypeDefinitions.assertDoubleIfAvailable(result.getImpactUnitaire(), entry, "impactUnitaire");
        DataTableTypeDefinitions.assertDoubleIfAvailable(result.getConsoElecMoyenne(), entry, "consoElecMoyenne");
        DataTableTypeDefinitions.assertStringIfAvailable(result.getNomSourceDonnee(), entry, "nomSourceDonnee");
        DataTableTypeDefinitions.assertStringIfAvailable(result.getUnite(), entry, "unite");
        DataTableTypeDefinitions.assertStringIfAvailable(result.getNomEquipementVirtuel(), entry, "nomEquipementVirtuel");
        DataTableTypeDefinitions.assertStringIfAvailable(result.getNomEquipement(), entry, "nomEquipement");
        DataTableTypeDefinitions.assertStringIfAvailable(result.getCluster(), entry, "cluster");
        DataTableTypeDefinitions.assertStringIfAvailable(result.getTrace(), entry, "trace");
    }
}
