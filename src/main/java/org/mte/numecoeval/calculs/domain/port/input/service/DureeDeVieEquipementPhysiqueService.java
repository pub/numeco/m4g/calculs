package org.mte.numecoeval.calculs.domain.port.input.service;

import org.mte.numecoeval.calculs.domain.data.demande.DemandeCalculImpactEquipementPhysique;
import org.mte.numecoeval.calculs.domain.data.trace.DureeDeVie;
import org.mte.numecoeval.calculs.domain.data.trace.DureeDeVieParDefaut;
import org.mte.numecoeval.calculs.domain.exception.CalculImpactException;

public interface DureeDeVieEquipementPhysiqueService {

    DureeDeVieParDefaut calculerDureeVieDefaut(DemandeCalculImpactEquipementPhysique demandeCalcul) throws CalculImpactException;

    DureeDeVie calculerDureeVie(DemandeCalculImpactEquipementPhysique demandeCalcul) throws CalculImpactException;
}
