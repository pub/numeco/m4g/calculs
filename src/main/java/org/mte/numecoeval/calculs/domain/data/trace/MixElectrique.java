package org.mte.numecoeval.calculs.domain.data.trace;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MixElectrique {
    private Double valeur;
    private boolean isServeur;
    private Double dataCenterPue;
    private Double valeurReferentielMixElectrique;
    private String sourceReferentielMixElectrique;
}
