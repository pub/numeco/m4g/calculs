package org.mte.numecoeval.calculs.domain.data.referentiel;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@AllArgsConstructor
@Getter
@Setter
public class ReferentielCorrespondanceRefEquipement {

    String modeleEquipementSource;

    String refEquipementCible;
}
