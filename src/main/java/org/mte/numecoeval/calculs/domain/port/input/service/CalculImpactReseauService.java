package org.mte.numecoeval.calculs.domain.port.input.service;

import org.mte.numecoeval.calculs.domain.data.demande.DemandeCalculImpactReseau;
import org.mte.numecoeval.calculs.domain.data.indicateurs.ImpactReseau;

public interface CalculImpactReseauService {

    String REF_RESEAU = "impactReseauMobileMoyen";

    ImpactReseau calculerImpactReseau(DemandeCalculImpactReseau demandeCalcul);

}
