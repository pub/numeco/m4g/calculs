package org.mte.numecoeval.calculs.domain.data.entree;


import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class EquipementVirtuel {
   private Long id;
   private String nomEquipementVirtuel;
   private String nomEquipementPhysique;
   private String nomSourceDonneeEquipementPhysique;
   private Integer vCPU;
   private String cluster;
   private String nomLot;
   private LocalDate dateLot;
   private String nomOrganisation;
   private String nomEntite;
   private String nomSourceDonnee;
   private Double consoElecAnnuelle;
   private String typeEqv;
   private Double capaciteStockage;
   private Double cleRepartition;
}
