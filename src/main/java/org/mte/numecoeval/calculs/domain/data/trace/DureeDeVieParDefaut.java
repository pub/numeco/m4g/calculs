package org.mte.numecoeval.calculs.domain.data.trace;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DureeDeVieParDefaut {
    private  Double valeur;
    private Double valeurEquipementDureeVieDefaut;
    private Double valeurReferentielHypothese;
    private String sourceReferentielHypothese;
}
