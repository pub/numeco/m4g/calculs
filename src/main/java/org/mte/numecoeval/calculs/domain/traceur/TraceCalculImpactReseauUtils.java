package org.mte.numecoeval.calculs.domain.traceur;

import org.mte.numecoeval.calculs.domain.data.demande.DemandeCalculImpactReseau;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielImpactReseau;
import org.mte.numecoeval.calculs.domain.data.trace.TraceCalculImpactReseau;
import org.mte.numecoeval.calculs.domain.exception.CalculImpactException;

public class TraceCalculImpactReseauUtils {
    private TraceCalculImpactReseauUtils() {
    }

    private static String getFormule(Float goTelecharge, String critere, String etapeACV, String equipementPhysique, Double impactReseauMobileMoyen) {
        return "impactReseau = 'equipementPhysique.goTelecharge (%s) x ref_ImpactReseau(%s, %s, %s).valeur(%s)'".formatted(goTelecharge, critere, etapeACV, equipementPhysique, impactReseauMobileMoyen);
    }

    public static TraceCalculImpactReseau buildTraceErreur(CalculImpactException exception) {
        var message = "Erreur lors du calcul de l'impact réseau pour un équipement physique";
        if(exception != null) {
            message = exception.getErrorType() + " : " + exception.getMessage();
        }
        return TraceCalculImpactReseau.builder()
                .erreur(message)
                .build();
    }

    public static TraceCalculImpactReseau buildTrace(DemandeCalculImpactReseau demandeCalculImpactReseau, ReferentielImpactReseau referentielImpactReseau){

        return TraceCalculImpactReseau.builder()
                .critere(demandeCalculImpactReseau.getCritere().getNomCritere())
                .etapeACV(demandeCalculImpactReseau.getEtape().getCode())
                .equipementPhysique(demandeCalculImpactReseau.getEquipementPhysique().getNomEquipementPhysique())
                .impactReseauMobileMoyen(referentielImpactReseau.getImpactReseauMobileMoyen().toString())
                .goTelecharge(demandeCalculImpactReseau.getEquipementPhysique().getGoTelecharge().toString())
                .sourceReferentielReseau(referentielImpactReseau.getSource())
                .formule(getFormule(
                        demandeCalculImpactReseau.getEquipementPhysique().getGoTelecharge(),
                        demandeCalculImpactReseau.getCritere().getNomCritere(),
                        demandeCalculImpactReseau.getEtape().getCode(),
                        referentielImpactReseau.getRefReseau(),
                        referentielImpactReseau.getImpactReseauMobileMoyen()
                ))
                .build();
    }
}
