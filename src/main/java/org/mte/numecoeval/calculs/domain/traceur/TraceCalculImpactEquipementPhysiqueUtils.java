package org.mte.numecoeval.calculs.domain.traceur;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.mte.numecoeval.calculs.domain.data.trace.ConsoElecAnMoyenne;
import org.mte.numecoeval.calculs.domain.data.trace.DureeDeVie;
import org.mte.numecoeval.calculs.domain.data.trace.MixElectrique;
import org.mte.numecoeval.calculs.domain.data.trace.TraceCalculImpactEquipementPhysique;
import org.mte.numecoeval.calculs.domain.exception.CalculImpactException;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TraceCalculImpactEquipementPhysiqueUtils {

    public static TraceCalculImpactEquipementPhysique buildTraceErreur(CalculImpactException exception) {
        var message = "Erreur lors du calcul de l'impact d'équipement physique";
        if(exception != null) {
            message = exception.getErrorType() + " : " + exception.getMessage();
        }
        return TraceCalculImpactEquipementPhysique.builder()
                .erreur(message)
                .build();
    }

    public static TraceCalculImpactEquipementPhysique buildTracePremierScenario(Double quantite, ConsoElecAnMoyenne consoElecAnMoyenne, MixElectrique mixElectriqueValeur) {
        return TraceCalculImpactEquipementPhysique.builder()
                .formule(getFormulePremierScenario(quantite,consoElecAnMoyenne.getValeur(),mixElectriqueValeur.getValeur()))
                .consoElecAnMoyenne(consoElecAnMoyenne)
                .mixElectrique(mixElectriqueValeur)
                .build();
    }

    public static TraceCalculImpactEquipementPhysique buildTraceSecondScenario(Double quantite, Double valeurRefrentiel, String sourceReferentiel, DureeDeVie dureeDeVie){
        return TraceCalculImpactEquipementPhysique.builder()
                .formule(getFormuleSecondScenario(quantite,valeurRefrentiel,dureeDeVie.getValeur()))
                .valeurReferentielImpactEquipement(valeurRefrentiel)
                .sourceReferentielImpactEquipement(sourceReferentiel)
                .dureeDeVie(dureeDeVie)
                .build();
    }
        public static String getFormulePremierScenario(Double quantite, Double consoElecAnMoyenne, Double mixElectriqueValeur){
        return "ImpactEquipementPhysique = (Quantité(%s) * ConsoElecAnMoyenne(%s) * MixElectrique(%s)) / 365".formatted(quantite,consoElecAnMoyenne,mixElectriqueValeur);
    }

    public static String getFormuleSecondScenario(Double quantite,Double valeurRefrentiel,Double dureeVie){
        return "ImpactEquipementPhysique = (Quantité(%s) * referentielImpactEquipement(%s)) / dureeVie(%s)".formatted(quantite,valeurRefrentiel,dureeVie);
    }

}
