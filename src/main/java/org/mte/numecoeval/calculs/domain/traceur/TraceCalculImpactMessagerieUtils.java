package org.mte.numecoeval.calculs.domain.traceur;

import org.mte.numecoeval.calculs.domain.data.demande.DemandeCalculImpactMessagerie;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielImpactMessagerie;
import org.mte.numecoeval.calculs.domain.data.trace.TraceCalculImpactMessagerie;
import org.mte.numecoeval.calculs.domain.exception.CalculImpactException;

public class TraceCalculImpactMessagerieUtils {

    private TraceCalculImpactMessagerieUtils() {
        // private constructor
    }

    public static TraceCalculImpactMessagerie buildTrace(DemandeCalculImpactMessagerie demandeCalcul, ReferentielImpactMessagerie referentielImpactMessagerie) {
        return TraceCalculImpactMessagerie.builder()
                .formule(getFormule(
                        demandeCalcul.getMessagerie().getVolumeTotalMailEmis()/demandeCalcul.getMessagerie().getNombreMailEmis(),
                        demandeCalcul.getMessagerie().getVolumeTotalMailEmis(),
                        demandeCalcul.getMessagerie().getNombreMailEmis(),
                        referentielImpactMessagerie.getConstanteCoefficientDirecteur(),
                        referentielImpactMessagerie.getConstanteOrdonneeOrigine(),
                        demandeCalcul.getMessagerie().getNombreMailEmisXDestinataires()
                ))
                .critere(demandeCalcul.getCritere().getNomCritere())
                .volumeTotalMailEmis(demandeCalcul.getMessagerie().getVolumeTotalMailEmis())
                .nombreMailEmis(demandeCalcul.getMessagerie().getNombreMailEmis())
                .nombreMailEmisXDestinataires(demandeCalcul.getMessagerie().getNombreMailEmisXDestinataires())
                .constanteCoefficientDirecteur(referentielImpactMessagerie.getConstanteCoefficientDirecteur())
                .constanteOrdonneeOrigine(referentielImpactMessagerie.getConstanteOrdonneeOrigine())
                .poidsMoyenMail(demandeCalcul.getMessagerie().getVolumeTotalMailEmis()/demandeCalcul.getMessagerie().getNombreMailEmis())
                .build();
    }

    public static TraceCalculImpactMessagerie buildTraceError(CalculImpactException exception) {
        var message = "Erreur lors du calcul de l'impact de messagerie";
        if(exception != null) {
            message = exception.getErrorType() + " : " + exception.getMessage();
        }
        return TraceCalculImpactMessagerie.builder()
                .erreur(message)
                .build();
    }

    private static String getFormule(Double poidsMoyenMail, Double volumeTotalMailEmis, Double nombreMailEmis, Double constanteCoefficientDirecteur, Double constanteOrdonneeOrigine, Double nombreMailEmisXDestinataires){
        return """
                poidsMoyenMail(%s) = volumeTotalMailEmis(%s)/nombreMailEmis(%s);
                impactMensuel = (constanteCoefficientDirecteur (%s) * poidsMoyenMail(%s) + constanteOrdonneeOrigine(%s)) * nombreMailEmisXDestinataires(%s)
                """.formatted(poidsMoyenMail,volumeTotalMailEmis,nombreMailEmis,constanteCoefficientDirecteur,poidsMoyenMail,constanteOrdonneeOrigine, nombreMailEmisXDestinataires);
    }
}
