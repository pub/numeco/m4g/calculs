package org.mte.numecoeval.calculs.domain.data.indicateurs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Builder
@AllArgsConstructor
@Getter
@Setter
public class ImpactReseau {
    LocalDateTime dateCalcul;
    String etapeACV;
    String critere;
    String nomLot;
    LocalDate dateLot;
    String nomOrganisation;
    String nomEquipement;
    String nomEntite;
    String nomSourceDonnee;


    String versionCalcul;
    String source;
    String statutIndicateur;
    String trace;
    String unite;
    Double impactUnitaire;
    String reference;

}
