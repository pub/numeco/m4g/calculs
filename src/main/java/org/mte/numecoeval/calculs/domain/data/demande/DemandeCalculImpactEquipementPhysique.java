package org.mte.numecoeval.calculs.domain.data.demande;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.mte.numecoeval.calculs.domain.data.entree.EquipementPhysique;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielCorrespondanceRefEquipement;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielCritere;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielEtapeACV;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielHypothese;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielImpactEquipement;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielMixElectrique;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielTypeEquipement;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Builder
@AllArgsConstructor
@Getter
@Setter
public class DemandeCalculImpactEquipementPhysique {

    LocalDateTime dateCalcul;

    EquipementPhysique equipementPhysique;

    ReferentielEtapeACV etape;

    ReferentielCritere critere;

    ReferentielTypeEquipement typeEquipement;

    ReferentielCorrespondanceRefEquipement correspondanceRefEquipement;

    List<ReferentielHypothese> hypotheses;

    List<ReferentielImpactEquipement> impactEquipements;

    List<ReferentielMixElectrique> mixElectriques;

    public Optional<ReferentielHypothese> getHypotheseFromCode(String code) {
        if(code == null) {
            return Optional.empty();
        }
        return CollectionUtils.emptyIfNull(hypotheses)
                .stream()
                .filter(hypothese -> code.equals(hypothese.getCode()))
                .findFirst();
    }

    public Optional<ReferentielImpactEquipement> getImpactEquipement(String refEquipement) {
        if(refEquipement == null) {
            return Optional.empty();
        }
        return CollectionUtils.emptyIfNull(impactEquipements)
                .stream()
                .filter(impactEquipement -> refEquipement.equals(impactEquipement.getRefEquipement())
                        && critere.getNomCritere().equals(impactEquipement.getCritere())
                        && etape.getCode().equals(impactEquipement.getEtape())
                )
                .findFirst();
    }

    public Optional<ReferentielMixElectrique> getMixElectrique(String pays) {
        if(pays == null) {
            return Optional.empty();
        }
        return CollectionUtils.emptyIfNull(mixElectriques)
                .stream()
                .filter(mixElectrique -> pays.equals(mixElectrique.getPays())
                        && critere.getNomCritere().equals(mixElectrique.getCritere())
                )
                .findFirst();
    }

    public String getRefEquipementCible() {
        if(correspondanceRefEquipement != null) {
            return correspondanceRefEquipement.getRefEquipementCible();
        }
        return null;
    }

    public String getRefEquipementParDefaut() {
        if(typeEquipement != null) {
            return typeEquipement.getRefEquipementParDefaut();
        }
        return null;
    }
}
