package org.mte.numecoeval.calculs.domain.data.referentiel;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ReferentielImpactReseau {

    private String refReseau;
    private String critere;
    private String etapeACV;
    private String unite;
    private String source;
    private Double impactReseauMobileMoyen;
    private Double consoElecMoyenne;


}
