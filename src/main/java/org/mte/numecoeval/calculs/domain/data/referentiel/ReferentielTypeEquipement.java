package org.mte.numecoeval.calculs.domain.data.referentiel;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@AllArgsConstructor
@Getter
@Setter
public class ReferentielTypeEquipement {
    String type;

    Boolean serveur;

    String commentaire;

    Double dureeVieDefaut;

    String source;

    String refEquipementParDefaut;
}
