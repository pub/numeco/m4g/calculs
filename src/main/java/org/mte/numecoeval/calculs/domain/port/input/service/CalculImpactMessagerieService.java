package org.mte.numecoeval.calculs.domain.port.input.service;

import org.mte.numecoeval.calculs.domain.data.demande.DemandeCalculImpactMessagerie;
import org.mte.numecoeval.calculs.domain.data.indicateurs.ImpactMessagerie;

public interface CalculImpactMessagerieService {

    ImpactMessagerie calculerImpactMessagerie(DemandeCalculImpactMessagerie demandeCalcul);

}
