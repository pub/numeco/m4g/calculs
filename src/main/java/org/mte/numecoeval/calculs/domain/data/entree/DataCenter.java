package org.mte.numecoeval.calculs.domain.data.entree;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class DataCenter {

   private String nomCourtDatacenter;
   private String nomLongDatacenter;
   private Double pue;
   private String localisation;
   private String nomLot;
   private LocalDate dateLot;
   private String nomOrganisation;
   private String nomEntite;
   private String nomSourceDonnee;
}
