package org.mte.numecoeval.calculs.domain.data.demande;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.mte.numecoeval.calculs.domain.data.entree.EquipementPhysique;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielCritere;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielEtapeACV;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielImpactReseau;

import java.time.LocalDateTime;
import java.util.List;

@Builder
@AllArgsConstructor
@Getter
@Setter
public class DemandeCalculImpactReseau {

    LocalDateTime dateCalcul;

    EquipementPhysique equipementPhysique;

    ReferentielEtapeACV etape;

    ReferentielCritere critere;

    List<ReferentielImpactReseau> impactsReseau;
}
