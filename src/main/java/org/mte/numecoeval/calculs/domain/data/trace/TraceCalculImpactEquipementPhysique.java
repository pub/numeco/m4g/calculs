package org.mte.numecoeval.calculs.domain.data.trace;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TraceCalculImpactEquipementPhysique {
    private String formule;
    private ConsoElecAnMoyenne consoElecAnMoyenne;
    private MixElectrique mixElectrique;
    private Double valeurReferentielImpactEquipement;
    private String sourceReferentielImpactEquipement;
    private DureeDeVie dureeDeVie;
    private String erreur;
}
