package org.mte.numecoeval.calculs.domain.data.demande;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.mte.numecoeval.calculs.domain.data.entree.Messagerie;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielCritere;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielImpactMessagerie;

import java.time.LocalDateTime;
import java.util.List;

@Builder
@AllArgsConstructor
@Getter
@Setter
public class DemandeCalculImpactMessagerie {

    LocalDateTime dateCalcul;

    Messagerie messagerie;

    ReferentielCritere critere;

    List<ReferentielImpactMessagerie> impactsMessagerie;
}
