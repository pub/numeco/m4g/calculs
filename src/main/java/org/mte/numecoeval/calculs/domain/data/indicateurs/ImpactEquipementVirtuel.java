package org.mte.numecoeval.calculs.domain.data.indicateurs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Builder
@AllArgsConstructor
@Getter
@Setter
public class ImpactEquipementVirtuel {
    LocalDateTime dateCalcul;
    String versionCalcul;
    String etapeACV;
    String critere;
    String source;
    String statutIndicateur;
    String trace;
    String nomLot;
    LocalDate dateLot;
    String unite;
    String nomOrganisation;
    String nomEntite;
    String nomEquipement;
    String nomSourceDonnee;
    String nomEquipementVirtuel;
    String cluster;
    Double impactUnitaire;
    Double consoElecMoyenne;
    Long idEntree;
}
